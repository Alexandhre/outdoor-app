@extends('layouts.master')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Contrato</h1>
        </div>
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->
  <hr>
  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header">
              <div class="card-tools">
                <button onclick="novoContrato()" type="button" class="btn btn-block btn-primary btn-sm">Novo Contrato</button>
              </div>
              <h3 class="card-title">Listagem de Contratos</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table class="table table-bordered">
                <thead>
                  <tr>
                    <th style="width: 10px">#</th>
                    <th>Cliente</th>
                    <th>Empresa</th>
                    <th style="width: 10px">Período</th>
                    <th style="width: 106px">Ações</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>1.</td>
                    <td>Update software</td>
                    <td>Update software</td>
                    <td><span class="badge bg-primary">55</span></td>
                    <td style="padding-left:32px;align-items: center;justify-content: center;">
                      <a href="{{ route('contratoEdit') }}">
                        <i style="margin-right: 10px;" class="nav-icon fa fa-pencil"></i>
                      </a>
                      <a>
                        <i data-toggle="modal" data-target="#modal-default" class="nav-icon fa fa-trash"></i>
                      </a>
                    </td>
                  </tr>
                  <tr>
                    <td>2.</td>
                    <td>Clean database</td>
                    <td>Update software</td>
                    <td><span class="badge bg-primary">70</span></td>
                    <td style="padding-left:32px;align-items: center;justify-content: center;">
                      <a href="{{ route('contratoEdit') }}">
                        <i style="margin-right: 10px;" class="nav-icon fa fa-pencil"></i>
                      </a>
                      <a>
                        <i data-toggle="modal" data-target="#modal-default" class="nav-icon fa fa-trash"></i>
                      </a>
                    </td>
                  </tr>
                  <tr>
                    <td>3.</td>
                    <td>Cron job running</td>
                    <td>Update software</td>
                    <td><span class="badge bg-primary">30</span></td>
                    <td style="padding-left:32px;align-items: center;justify-content: center;">
                      <a href="{{ route('contratoEdit') }}">
                        <i style="margin-right: 10px;" class="nav-icon fa fa-pencil"></i>
                      </a>
                      <a>
                        <i data-toggle="modal" data-target="#modal-default" class="nav-icon fa fa-trash"></i>
                      </a>
                    </td>
                  </tr>
                  <tr>
                    <td>4.</td>
                    <td>Fix and squish bugs</td>
                    <td>Update software</td>
                    <td><span class="badge bg-primary">90</span></td>
                    <td style="padding-left:32px;align-items: center;justify-content: center;">
                      <a href="{{ route('contratoEdit') }}">
                        <i style="margin-right: 10px;" class="nav-icon fa fa-pencil"></i>
                      </a>
                      <a>
                        <i data-toggle="modal" data-target="#modal-default" class="nav-icon fa fa-trash"></i>
                      </a>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
            <div class="card-footer clearfix">
              <ul class="pagination pagination-sm m-0 float-right">
                <li class="page-item"><a class="page-link" href="#">&laquo;</a></li>
                <li class="page-item"><a class="page-link" href="#">1</a></li>
                <li class="page-item"><a class="page-link" href="#">2</a></li>
                <li class="page-item"><a class="page-link" href="#">3</a></li>
                <li class="page-item"><a class="page-link" href="#">&raquo;</a></li>
              </ul>
            </div>
          </div>
          <!-- /.card -->
        </div>
      </div>
  </section>
  <!-- /.content -->
  <div class="modal fade" id="modal-default">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Exclusão</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <p>Tem certeza que deseja excluir esse contrato?</p>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Sair</button>
          <button id="btn-confirmar" type="button" class="btn btn-primary">Confirmar</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->
</div>
<!-- /.content-wrapper -->
@endsection

@section('javascript')
<!-- jQuery -->
<script src="/dist/plugins/jquery/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="/dist/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="/dist/plugins/morris/morris.min.js"></script>
<!-- Sparkline -->
<script src="/dist/plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="/dist/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="/dist/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="/dist/plugins/knob/jquery.knob.js"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
<script src="/dist/plugins/daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="/dist/plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="/dist/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="/dist/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="/dist/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="/dist/js/adminlte.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="/dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="/dist/js/demo.js"></script>

<script>
  $(document).ready(function() {
    $("#btn-confirmar").click(function() {
      $.ajax({
        url: '/vincularServico',
        type: 'POST',
        data: {
          ServicoId: 1
        },
        dataType: 'JSON',
        success: function(data) {
          alert();
        },
        error: function(e) {
          alert('Não foi possível obter os dados');
        }
      });
    });



  });

  function novoContrato() {
    window.location.href = "{{ route('contratoEdit') }}";
  }
</script>

@stop