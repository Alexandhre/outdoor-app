@extends('layouts.master')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
 <!-- Content Header (Page header) -->
 <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Outdoor</h1>
        </div>      
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->
  <hr>
  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="card card-light">
            <!-- /.card-header -->
            <!-- form start -->
            <div role="form">
              <div class="card-body">
                <div class="form-group">
                  <label>Contratante</label>
                  <select class="form-control select2" style="width: 100%;">
                    <option selected="selected">Agência 1</option>
                    <option>Agência 2</option>
                    <option>Agência 3</option>
                    <option>Agência 4</option>
                  </select>
                </div>
                <div class="form-group">
                  <label>Dispositivo</label>
                  <select class="form-control select2" style="width: 100%;">
                    <option selected="selected">Dispositivo 1</option>
                    <option>Dispositivo 2</option>
                    <option>Dispositivo 3</option>
                    <option>Dispositivo 4</option>
                  </select>
                </div>
                <!-- /.form group -->
                <div class="form-group">
                  <label for="exampleInputEmail1">Endereço</label>
                  <input type="text" class="form-control" id="endereco" placeholder="Digite o endereço">
                </div>
                <!-- /.form group -->
                <div class="form-group">
                  <label for="exampleInputEmail1">CEP</label>
                  <input type="text" class="form-control" id="cep" placeholder="Digite o CEP">
                </div>
              </div>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
              <button onclick="indexOutdoor()" type="submit" class="btn btn-default">Voltar</button>
              <button type="submit" class="btn btn-primary">Salvar</button>
            </div>
          </div>
        </div>
        <!-- /.card -->

      </div>
    </div>
  </section>
</div>
<!-- /.content-wrapper -->
@endsection

@section('javascript')
<!-- jQuery -->
<script src="/dist/plugins/jquery/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="/dist/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="/dist/plugins/morris/morris.min.js"></script>
<!-- Sparkline -->
<script src="/dist/plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="/dist/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="/dist/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="/dist/plugins/knob/jquery.knob.js"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
<script src="/dist/plugins/daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="/dist/plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="/dist/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="/dist/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="/dist/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="/dist/js/adminlte.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="/dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="/dist/js/demo.js"></script>

<script>
  $(document).ready(function() {

    $("#btn-confirmar").click(function() {
      $.ajax({
        url: '/vincularServico',
        type: 'POST',
        data: {
          ServicoId: 1
        },
        dataType: 'JSON',
        success: function(data) {
          alert();
        },
        error: function(e) {
          alert('Não foi possível obter os dados');
        }
      });
    });

  });

  function indexOutdoor() {
    window.location.href = "{{ route('outdoor') }}";
  }
</script>

@stop