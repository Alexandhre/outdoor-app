@extends('layouts.master')
@section('content')
<div class="content-wrapper">
  <div class="content-header">
    <div class="container-fluid">
    </div>
    <section class="content">
      <div class="container-fluid">
        <div style="display: flex;align-items: center;justify-content: center;" class="row">
          <div class="col-lg-10 col-6">
            <div class="small-box bg-warning">
              <div style="color: white" class="inner">
                <h4 style="color: white;" class="card-title p-3">
                  <svg width="43" height="43" viewBox="0 0 43 43" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M43 35.909V30.6708L40.9936 30.1581C40.9797 30.1242 40.9658 30.0904 40.9512 30.0568L42.0075 28.2766L38.3034 24.5725L36.5231 25.6287C36.4889 25.6141 36.455 25.6003 36.4218 25.5864L35.9091 23.5806H30.6709L30.1582 25.587C30.1244 25.6009 30.0905 25.6148 30.057 25.6294L28.2767 24.5731L24.5726 28.2773L25.6288 30.0575C25.6143 30.0917 25.6004 30.1256 25.5865 30.1588L23.5807 30.6715V35.9097L25.5872 36.4224C25.6011 36.4563 25.6149 36.4901 25.6295 36.5236L24.5733 38.3039L28.2774 42.008L30.0576 40.9518C30.0918 40.9663 30.1257 40.9802 30.1589 40.9941L30.6716 42.9999H35.9098L36.4225 40.9934C36.4564 40.9796 36.4902 40.9657 36.5238 40.9511L38.304 42.0073L42.0081 38.3032L40.9519 36.523C40.9665 36.4888 40.9804 36.4549 40.9942 36.4217L43 35.909ZM40.2655 38.0848L38.0849 40.2653L36.6027 39.3859L36.2691 39.5454C36.0541 39.6473 35.8343 39.7394 35.6081 39.8194L35.2586 39.9433L34.8329 41.6128H31.7485L31.3228 39.9433L30.9733 39.8194C30.7471 39.7401 30.5267 39.648 30.3123 39.5454L29.9787 39.3859L28.4965 40.2653L26.3159 38.0848L27.1954 36.6025L27.0359 36.269C26.934 36.0539 26.8419 35.8342 26.7619 35.6079L26.638 35.2585L24.9678 34.8328V31.7484L26.6373 31.3227L26.7613 30.9732C26.8405 30.747 26.9326 30.5265 27.0352 30.3122L27.1947 29.9786L26.3153 28.4964L28.4958 26.3158L29.9781 27.1953L30.3116 27.0358C30.5267 26.9338 30.7465 26.8417 30.9727 26.7618L31.3222 26.6379L31.7478 24.9677H34.8322L35.2579 26.6372L35.6074 26.7611C35.8336 26.8404 36.0541 26.9325 36.2684 27.0351L36.602 27.1946L38.0842 26.3151L40.2648 28.4957L39.3853 29.9779L39.5448 30.3115C39.6468 30.5265 39.7389 30.7463 39.8188 30.9725L39.9427 31.322L41.6129 31.7477V34.8321L39.9434 35.2578L39.8195 35.6073C39.7402 35.8335 39.6481 36.0539 39.5455 36.2683L39.386 36.6019L40.2655 38.0848Z" fill="white" />
                    <path d="M33.2903 27.7417C30.231 27.7417 27.7419 30.2308 27.7419 33.2901C27.7419 36.3494 30.231 38.8385 33.2903 38.8385C36.3497 38.8385 38.8387 36.3494 38.8387 33.2901C38.8387 30.2308 36.3497 27.7417 33.2903 27.7417ZM33.2903 37.4514C30.9953 37.4514 29.129 35.5851 29.129 33.2901C29.129 30.9951 30.9953 29.1288 33.2903 29.1288C35.5853 29.1288 37.4516 30.9951 37.4516 33.2901C37.4516 35.5851 35.5853 37.4514 33.2903 37.4514Z" fill="white" />
                    <path d="M33.2903 30.5161C31.7603 30.5161 30.5161 31.7603 30.5161 33.2903C30.5161 34.8203 31.7603 36.0645 33.2903 36.0645C34.8203 36.0645 36.0645 34.8203 36.0645 33.2903C36.0645 31.7603 34.8203 30.5161 33.2903 30.5161ZM33.2903 34.6774C32.5253 34.6774 31.9032 34.0553 31.9032 33.2903C31.9032 32.5253 32.5253 31.9032 33.2903 31.9032C34.0553 31.9032 34.6774 32.5253 34.6774 33.2903C34.6774 34.0553 34.0553 34.6774 33.2903 34.6774Z" fill="white" />
                    <path d="M17.5642 11.7433C17.7965 12.4243 18.2009 13.0247 18.7258 13.4964V14.0847L15.428 15.3211C14.0819 15.8253 13.1774 17.1301 13.1774 18.5677V22.1935H29.8225V18.5677C29.8225 17.1301 28.918 15.8253 27.5726 15.3204L24.2741 14.084V13.4964C24.799 13.0254 25.2034 12.4249 25.4357 11.7433C26.7425 11.5191 27.7419 10.3853 27.7419 9.01613V6.24194C27.7419 2.80061 24.942 0 21.4999 0C18.0579 0 15.258 2.80061 15.258 6.24194V9.01613C15.258 10.3853 16.2574 11.5191 17.5642 11.7433ZM22.887 14.3725L21.4999 16.6841L20.1129 14.3725V14.3217C20.5477 14.4765 21.013 14.5645 21.4999 14.5645C21.9869 14.5645 22.4522 14.4758 22.887 14.3217V14.3725ZM19.1216 15.4169L20.5199 17.7464L19.1308 18.4407L18.2334 15.7498L19.1216 15.4169ZM28.4354 18.5677V20.8065H14.5645V18.5677C14.5645 17.7048 15.107 16.9225 15.9146 16.6194L16.9343 16.2374L18.3207 20.3974L21.4999 18.8078L24.6792 20.3974L26.0656 16.2374L27.0859 16.6201C27.8929 16.9225 28.4354 17.7048 28.4354 18.5677ZM24.7665 15.7498L23.8691 18.4413L22.48 17.7471L23.8783 15.4169L24.7665 15.7498ZM21.4999 13.1774C19.9699 13.1774 18.7258 11.9332 18.7258 10.4032V7.58196C19.741 7.47495 20.699 7.10481 21.4999 6.47492C22.3008 7.10548 23.2589 7.47495 24.2741 7.58196V10.4032C24.2741 11.9332 23.03 13.1774 21.4999 13.1774ZM25.6612 10.2105V7.8224C26.074 8.06318 26.3548 8.50579 26.3548 9.01681C26.3548 9.52782 26.074 9.96976 25.6612 10.2105ZM21.4999 1.3871C24.1763 1.3871 26.3548 3.56493 26.3548 6.24194V6.62765C25.945 6.38891 25.4753 6.24194 24.9677 6.24194H24.8485C23.7685 6.24194 22.7533 5.82167 21.9903 5.05803L21.4999 4.56699L21.0096 5.05803C20.2459 5.82167 19.2314 6.24194 18.1514 6.24194H18.0322C17.5246 6.24194 17.0549 6.38891 16.6451 6.62765V6.24194C16.6451 3.56493 18.8236 1.3871 21.4999 1.3871ZM17.3387 7.82172V10.2099C16.9258 9.96908 16.6451 9.52647 16.6451 9.01545C16.6451 8.50443 16.9258 8.0625 17.3387 7.82172Z" fill="white" />
                    <path d="M0 43H13.871V24.9678H0V43ZM1.3871 26.3549H12.4839V41.6129H1.3871V26.3549Z" fill="white" />
                    <path d="M2.77417 27.7417H11.0968V29.1288H2.77417V27.7417Z" fill="white" />
                    <path d="M2.77417 30.5161H4.16127V31.9032H2.77417V30.5161Z" fill="white" />
                    <path d="M5.54834 30.5161H11.0967V31.9032H5.54834V30.5161Z" fill="white" />
                    <path d="M2.77417 33.2905H11.0968V34.6776H2.77417V33.2905Z" fill="white" />
                    <path d="M2.77417 36.0645H11.0968V37.4516H2.77417V36.0645Z" fill="white" />
                    <path d="M9.70972 38.8389H11.0968V40.226H9.70972V38.8389Z" fill="white" />
                    <path d="M2.77417 38.8389H8.32256V40.226H2.77417V38.8389Z" fill="white" />
                    <path d="M18.5226 31.0066L17.5419 30.0259L14.2773 33.2904L17.5419 36.555L18.5226 35.5743L16.9323 33.984H22.1935V32.5969H16.9323L18.5226 31.0066Z" fill="white" />
                    <path d="M7.62904 11.097C7.62904 10.715 7.93992 10.4034 8.32259 10.4034H11.5032L9.91287 11.9937L10.8936 12.9744L14.1581 9.70987L10.8936 6.44531L9.91287 7.42603L11.5032 9.01632H8.32259C7.17559 9.01632 6.24194 9.94997 6.24194 11.097V23.5808H7.62904V11.097Z" fill="white" />
                    <path d="M33.2903 11.0968V19.8257L31.7 18.2354L30.7193 19.2162L33.9839 22.4807L37.2484 19.2162L36.2677 18.2354L34.6774 19.8257V11.0968C34.6774 9.94976 33.7438 9.01611 32.5968 9.01611H29.129V10.4032H32.5968C32.9794 10.4032 33.2903 10.7148 33.2903 11.0968Z" fill="white" />
                  </svg>
                  <span>
                    Olá, clique aqui para criar uma conta e fazer seus próximos requerimentos de forma mais rápida e prática.
                  </span>
                </h4>
              </div>
            </div>
          </div>
        </div>

        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Serviços Online</h1>
          </div>
          </hr>
        </div>
        <hr>
        <div class="row">
          <section class="col-lg-12 connectedSortable">
            <div style="background: #5d9269;" class="card">
              <div class="card-header d-flex p-0">
                <h3 style="color: white;" class="card-title p-3">
                  <svg data-widget="collapse" data-toggle="tooltip" title="Collapse" style="margin-right: 10px;" width="17" height="10" viewBox="0 0 17 10" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M8.49611 1.69823L15.7562 8.95827C16.0072 9.20925 16.4142 9.20925 16.6652 8.95827C16.9161 8.70721 16.9161 8.30027 16.6652 8.04925L8.95062 0.334696C8.6996 0.0837468 8.29263 0.0837468 8.0416 0.334696L0.327041 8.04925C0.0803868 8.30464 0.0874687 8.71161 0.342863 8.95827C0.592004 9.19886 0.986961 9.19886 1.23606 8.95827L8.49611 1.69823Z" fill="white" />
                  </svg>
                  Viabilidade
                </h3>
              </div>
              <div class="card-body">
                <div class="tab-content p-0">
                  <ul class="list-group">
                    <li style="background-color: transparent; color: white;" class="list-group-item">Analise Orientação Prévia - AOP</li>
                    <li style="background-color: transparent; color: white;" class="list-group-item">Consulta Prévia</li>
                  </ul>
                </div>
              </div>

            </div>
          </section>
        </div>
      </div>

    </section>

  </div>
  @endsection

  @section('javascript')
  <script src="/dist/plugins/jquery/jquery.min.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
  <script>
    $.widget.bridge('uibutton', $.ui.button)
  </script>
  <script src="/dist/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
  <script src="/dist/plugins/morris/morris.min.js"></script>
  <script src="/dist/plugins/sparkline/jquery.sparkline.min.js"></script>
  <script src="/dist/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
  <script src="/dist/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
  <script src="/dist/plugins/knob/jquery.knob.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
  <script src="/dist/plugins/daterangepicker/daterangepicker.js"></script>
  <script src="/dist/plugins/datepicker/bootstrap-datepicker.js"></script>
  <script src="/dist/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
  <script src="/dist/plugins/slimScroll/jquery.slimscroll.min.js"></script>
  <script src="/dist/plugins/fastclick/fastclick.js"></script>
  <script src="/dist/js/adminlte.js"></script>
  <script src="/dist/js/pages/dashboard.js"></script>
  <script src="/dist/js/demo.js"></script>
  @stop