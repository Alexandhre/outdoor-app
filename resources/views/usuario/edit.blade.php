@extends('layouts.master')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Usuário</h1>
        </div>      
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->
  <hr>
  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="card card-light">
            <!-- /.card-header -->
            <!-- form start -->


            <div role="form">
              <div class="card-body">
                <form id="edit-form" action="{{ route('Editar') }}" method="POST" style="display: block;">
                  @csrf
                  <input type="text" style="display: none;" class="form-control" id="id_usuario" name="id_usuario"  value="{{$user->id_usuario}}">

                  <div class="form-group">
                    <label for="exampleInputEmail1">Nome</label>
                    <input type="text" class="form-control" id="nome" name="nome" placeholder="Digite o Nome" value="{{$user->nome}}">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Email</label>
                    <input type="text" class="form-control" id="email" name="email" placeholder="Digite o Email" value="{{$user->email}}">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">CPF</label>
                    <input type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" id="cpf" name="cpf" value="{{$user->cpf}}" placeholder="Digite o CPF" >
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">RG</label>
                    <input type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" id="rg" name="rg" value="{{$user->rg}}" placeholder="Digite o RG">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Data de Nascimento</label>
                    <input type="date" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" id="nrNascimento" name="nrNascimento" value="{{$user->nrNascimento}}" placeholder="Digite a dta de nascimento">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Endereço</label>
                    <input type="text" class="form-control" id="endereco" name="endereco" placeholder="Digite o Endereço" value="{{$user->endereco}}">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">CEP</label>
                    <input type="text" class="form-control" id="cep" name="cep" placeholder="Digite o CEP" value="{{$user->cep}}">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Telefone</label>
                    <input type="text" class="form-control" id="telefone" name="telefone"  placeholder="Digite o Telefone" value="{{$user->telefone}}">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Senha</label>
                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Password">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Confirmar Senha</label>
                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Confirmation Password">
                    <div class="input-group-append">
                      @if ($errors->has('password'))
                        <span class="invalid-feedback" role="alert">
                                                          <strong>{{ $errors->first('password') }}</strong>
                                                      </span>
                      @endif
                    </div>
                  </div>
                  <div class="form-group">
                    <label>Perfil</label>
                    <select class="form-control select2" name="perfil" style="width: 100%;">

                      @foreach($perfil as $i)
                        @if($i->id_perfil == $user->id_perfil)
                          <option value="{{$i->id_perfil}}" selected>{{$i->descricao}}</option>
                        @else
                          <option value="{{$i->id_perfil}}">{{$i->descricao}}</option>
                        @endif

                      @endforeach
                    </select>
                  </div>
                </form>
              </div>

              <!-- /.card-body -->
              <div class="card-footer">
                <button onclick="indexOutdoor()" type="submit" class="btn btn-default">Voltar</button>

                <button href="{{ route('Editar') }}"  onclick="event.preventDefault();
                        document.getElementById('edit-form').submit();" class="btn btn-primary">Salvar</button>
              </div>
            </div>
          </div>
          <!-- /.card -->
        </div>
      </div>
  </section>
</div>
<!-- /.content-wrapper -->
@endsection

@section('javascript')
<!-- jQuery -->
<script src="/dist/plugins/jquery/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="/dist/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="/dist/plugins/morris/morris.min.js"></script>
<!-- Sparkline -->
<script src="/dist/plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="/dist/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="/dist/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="/dist/plugins/knob/jquery.knob.js"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
<script src="/dist/plugins/daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="/dist/plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="/dist/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="/dist/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="/dist/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="/dist/js/adminlte.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="/dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="/dist/js/demo.js"></script>

<script>
  $(document).ready(function() {

    $("#btn-confirmar").click(function() {
      $.ajax({
        url: '/vincularServico',
        type: 'POST',
        data: {
          ServicoId: 1
        },
        dataType: 'JSON',
        success: function(data) {
          alert();
        },
        error: function(e) {
          alert('Não foi possível obter os dados');
        }
      });
    });

  });

  function indexOutdoor() {
    window.location.href = "{{ route('usuario') }}";
  }
</script>

@stop