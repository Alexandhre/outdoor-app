<div class="modal fade" id="modal-register">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Registro</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <!-- Main content -->
                <section class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <!-- left column -->
                            <div class="col-md-12">

                                <!-- form start -->
                                <div>
                                    <div class="card-body">
                                        <form id="register-form" action="{{ route('create') }}" method="post">
                                            @csrf

                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Nome</label>
                                                <input type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"  id="nome" name="nome" value="{{ old('name') }}" placeholder="Digite o Nome"  required>
                                                @if ($errors->has('name'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('name') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Email</label>
                                                <input type="email" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" id="email" name="email" value="{{ old('email') }}" placeholder="Digite o Email" required>
                                                <div class="input-group-append">
                                                    @if ($errors->has('email'))
                                                        <span class="invalid-feedback" role="alert">
                                                          <strong>{{ $errors->first('email') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">CPF</label>
                                                <input type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" id="cpf" name="cpf" value="{{ old('cpf') }}" placeholder="Digite o CPF" required>
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">RG</label>
                                                <input type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" id="rg" name="rg" value="{{ old('rg') }}" placeholder="Digite o RG">
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Data de Nascimento</label>
                                                <input type="date" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" id="nrNascimento" name="nrNascimento" value="{{ old('nrNascimento') }}" placeholder="Digite a dta de nascimento">
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Endereço</label>
                                                <input type="text" class="form-control" id="endereco" name="endereco" placeholder="Digite o Endereço">
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">CEP</label>
                                                <input type="text" class="form-control" id="cep" name="cep" placeholder="Digite o CEP">
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Telefone</label>
                                                <input type="text" class="form-control" id="telefone" name="telefone"  placeholder="Digite o Telefone">
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Senha</label>
                                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Password" required>
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Confirmar Senha</label>
                                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Confirmation Password" required>
                                                <div class="input-group-append">
                                                    @if ($errors->has('password'))
                                                        <span class="invalid-feedback" role="alert">
                                                          <strong>{{ $errors->first('password') }}</strong>
                                                      </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label>Perfil</label>
                                                <select class="form-control select2" name="perfil" style="width: 100%;">

                                                    @foreach($perfil as $i)
                                                    <option value="{{$i->id_perfil}}">{{$i->descricao}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="modal-footer justify-content-between" style="margin-top: 15%; margin-bottom: -10%;">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Sair</button>

                                                <button type="submit" class="btn btn-primary">Registrar</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <!-- /.card -->
                            </div>
                        </div>
                    </div>
                </section>

            </div>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->

</div>
<!-- /.content-wrapper -->

@section('javascript')
    <!-- jQuery -->
    <script src="/dist/plugins/jquery/jquery.min.js"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
        $.widget.bridge('uibutton', $.ui.button)
    </script>
    <!-- Bootstrap 4 -->
    <script src="/dist/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- Morris.js charts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
    <script src="/dist/plugins/morris/morris.min.js"></script>
    <!-- Sparkline -->
    <script src="/dist/plugins/sparkline/jquery.sparkline.min.js"></script>
    <!-- jvectormap -->
    <script src="/dist/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
    <script src="/dist/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
    <!-- jQuery Knob Chart -->
    <script src="/dist/plugins/knob/jquery.knob.js"></script>
    <!-- daterangepicker -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
    <script src="/dist/plugins/daterangepicker/daterangepicker.js"></script>
    <!-- datepicker -->
    <script src="/dist/plugins/datepicker/bootstrap-datepicker.js"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script src="/dist/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
    <!-- Slimscroll -->
    <script src="/dist/plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="/dist/plugins/fastclick/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="/dist/js/adminlte.js"></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <script src="/dist/js/pages/dashboard.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="/dist/js/demo.js"></script>

    <script>
        $(document).ready(function() {

            $("#btn-confirmar").click(function() {
                $.ajax({
                    url: '/vincularServico',
                    type: 'POST',
                    data: {
                        ServicoId: 1
                    },
                    dataType: 'JSON',
                    success: function(data) {
                        alert();
                    },
                    error: function(e) {
                        alert('Não foi possível obter os dados');
                    }
                });
            });

        });

        function indexOutdoor() {
            window.location.href = "{{ route('usuario') }}";
        }
    </script>
@stop