<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'usuarios';

    protected $fillable = [
        'id_usuario'
        ,'nome'
        ,'email'
        ,'senha'
        ,'cpf'
        ,'rg'
        ,'nr_nascimento'
        ,'endereco'
        ,'cep'
        ,'telefone'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getAuthPassword()
    {
        return $this->senha;
    }

    public static function getUsers(){
        $users = User::join('usuario_perfil','usuarios.id_usuario','usuario_perfil.id_usuario')
            ->join('perfil','usuario_perfil.id_perfil','perfil.id_perfil')
            ->get();

        return $users;
    }

    public static function getUserById($id){
        $user = User::join('usuario_perfil','usuarios.id_usuario','usuario_perfil.id_usuario')
            ->where('usuarios.id_usuario', $id)
            ->first();

        return $user;
    }

    public static function updateUser($data){

        $user = User::join('usuario_perfil','usuarios.id_usuario','usuario_perfil.id_usuario')
            ->where('usuarios.id_usuario', $data['id_usuario'])
            ->update([
                'nome' => $data['nome'],//
                'email' => $data['email'],//
                'senha' => Hash::make($data['password']),//
                'cpf'  => $data['cpf'],
                'rg'  => $data['rg'],
                'nr_nascimento'  => $data['nrNascimento'],
                'endereco'  => $data['endereco'],//
                'cep'  => $data['cep'],//
                'telefone'  => $data['telefone']//
                ,'usuario_perfil.id_perfil' =>$data['perfil']
            ]);

        return $user;
    }

    public static function deletUsers($id){

        DB::table('usuario_perfil')->where('id_usuario', $id)->delete();

        $users = User::where('id_usuario', $id)
            ->delete();

        return $users;
    }

}
