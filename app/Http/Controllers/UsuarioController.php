<?php

namespace App\Http\Controllers;

use App\Perfil;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UsuarioController extends Controller
{
    public function index()
    {
        $users = User::getUsers();
        $perfil = Perfil::get();
        return view('usuario.index',compact(['users','perfil']));
    }

    public function edit(Request $request)
    {

        $user = User::getUserById($request->all()['id_usuario']);

        $perfil = Perfil::get();
        return view('usuario.edit',compact(['user','perfil']));
    }

    public function editar(Request $request)
    {

        $user = User::updateUser($request->all());

        return redirect()->route('usuario');
    }
    public function delet(Request $request)
    {

        $user = User::deletUsers($request->all()['id_usuario']);

        return redirect()->route('usuario');
    }

    public function create(Request $request)
    {
        $data = $request->all();

        try{
            $user = User::insert([
                'nome' => $data['nome'],//
                'email' => $data['email'],//
                'senha' => Hash::make($data['password']),//
                'cpf'  => $data['cpf'],
                'rg'  => $data['rg'],
                'nr_nascimento'  => $data['nrNascimento'],
                'endereco'  => $data['endereco'],//
                'cep'  => $data['cep'],//
                'telefone'  => $data['telefone']//
            ]);

        }catch (Exception $e){
            return back()->withErrors([
                'name' => 'Dados incorrentos'
            ])->withInput(\request(['name']));
        };

        $id = User::select('id_usuario')->where('email',$data['email'] )->first();

        DB::table('usuario_perfil')->insert([
            'id_usuario' => $id->id_usuario
            ,'id_perfil' => $data['perfil']
        ]);

        return redirect()->route('usuario');
    }

    /**
     * Store a new user.
     *
     * @param  Request  $request
     * @return Response
     */
    public function vincularServico(Request $request)
    {        
        dd($request);
        return "";
    }


}
