<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    // use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['only'=> 'showLoginForm']);
    }

    public function showLoginForm(){
        if(key_exists('email',session()->all())){

            return Redirect()->route('home');
        }else{

            return view('auth.login');
        }
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|string
     * @throws \Illuminate\Validation\ValidationException
     */
    public function login(){
        Session()->flush();
        Auth()->logout();
        $crendentials = $this->validate(request(),[
            'email' => 'email|required|string',
            'password' => 'required|string',
        ]);

        // Get user by email
        $company = User::join('usuario_perfil','usuarios.id_usuario','usuario_perfil.id_usuario')
            ->where('usuarios.email', $crendentials['email'])
            ->first();

        // Validate Company
        if(!$company) {
            return back()->withErrors([
                'email' => 'email ou senha errados'
            ])->withInput(\request(['email']));
        }

        if (Auth::attempt( array (
            'email' => $crendentials['email'],
            'password' =>  $crendentials['password']
        ) )) {
            session()->flush(); // Removes a specific variable
            session ([
                'email' => $crendentials['email'],
                'nome' => $company->nome,
                'id_perfil' => $company->id_perfil
            ]);

            return Redirect()->route('home');
        } else {

            return back()->withErrors([
                'email' => 'email ou senha errados'
            ])->withInput(\request(['email']));
        }
    }

    public function logout() {
        Session()->flush();
        Auth()->logout();

        return redirect('/login');
    }
}
