<?php

namespace App\Http\Controllers\Auth;

use App\Administrador;
use App\Perfil;
use App\User;
use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/registro';
    //protected $redirectTo = redirect()->back()->with('alert','hello')'/registro';


    public function showRegisterForm(){
        if(key_exists('email',session()->all())){
            return view('landpage');
        }else{

            $perfil = Perfil::get();

            return view('auth.register', compact('perfil'));
        }

    }


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {

        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {

        try{
            $user = User::insert([
                'nome' => $data['nome'],//
                'email' => $data['email'],//
                'senha' => Hash::make($data['password']),//
                'cpf'  => $data['cpf'],
                'rg'  => $data['rg'],
                'nr_nascimento'  => $data['nrNascimento'],
                'endereco'  => $data['endereco'],//
                'cep'  => $data['cep'],//
                'telefone'  => $data['telefone']//
            ]);

        }catch (Exception $e){
            return back()->withErrors([
                'name' => 'Dados incorrentos'
            ])->withInput(\request(['name']));
        };

        $id = User::select('id_usuario')->where('email',$data['email'] )->first();

        DB::table('usuario_perfil')->insert([
            'id_usuario' => $id->id_usuario
            ,'id_perfil' => $data['perfil']
        ]);

        return redirect()->route('usuario');

//        if ($contabil_user) {
//            try {
//                $invitedUser = new User;
//                $invitedUser->email = $data['email'];
//
//                $invitedUser->notify(
//                    new EmailValidade($contabil_user)
//                );
//
//
//            } catch (Exception $e) {
//                return back()->withErrors([
//                    'email' => 'erro ao validar email'
//                ])->withInput(\request(['email']));
//            }
//
//        }
        return $user;

    }
}
