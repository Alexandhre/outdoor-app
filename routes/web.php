<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Middleware\Session;
//use Illuminate\Support\Facades\Redirect;

Route::get('/', function () {
    return Redirect()->route('home');
});

Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');
Route::get('/registro', 'Auth\RegisterController@showRegisterForm')->name('registro');
//Route::get('/home', 'Auth\LoginController@showLoginForm')->name('home');
Route::post('login', 'Auth\LoginController@login')->name('login');
Route::post('register', 'Auth\RegisterController@create')->name('register');
Route::get('/logout', 'Auth\LoginController@logout')->name('logout');

//Auth::routes();


Route::group(['middleware' => Session::class], function () {
    Route::get('/outapp/home', 'DashboardController@versionone')->name('home');
    Route::get('/outapp/v2', 'DashboardController@versiontwo')->name('v2');
    Route::get('/outapp/v3', 'DashboardController@versionthree')->name('v3');

    Route::get('/outapp/outdoor', 'OutdoorController@index')->name('outdoor');
    Route::get('/outapp/outdoorEdit', 'OutdoorController@edit')->name('outdoorEdit');

    Route::get('/outapp/usuario', 'UsuarioController@index')->name('usuario');

    Route::post('/outapp/usuarioEdit', 'UsuarioController@edit')->name('usuarioEdit');
    Route::post('/outapp/usuarioEditar', 'UsuarioController@editar')->name('Editar');
    Route::post('/outapp/usuarioNew', 'UsuarioController@create')->name('create');
    Route::post('/outapp/usuarioDeletar', 'UsuarioController@delet')->name('usuarioDeletar');

    Route::get('/outapp/contrato', 'ContratoController@index')->name('contrato');
    Route::get('/outapp/contratoEdit', 'ContratoController@edit')->name('contratoEdit');
});

Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');
